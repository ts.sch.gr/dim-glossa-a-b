_global.RGB = function(r, g, b) {
	var col = (r << 16 | g << 8 | b);
	return col;
};
_global.draw_rect = function(obj, l, t, r, b, line_obj, fill_obj) {
	obj = (obj == undefined) ? _root : obj;
	with (obj) {
		if (line_obj != undefined) {
			lineStyle(line_obj.size, line_obj.color, line_obj.alpha);
		}
		if (fill_obj != undefined) {
			beginFill(fill_obj.color, fill_obj.alpha);
		}
		moveTo(l, t);
		lineTo(r, t);
		lineTo(r, b);
		lineTo(l, b);
		lineTo(l, t);
		if (fill_obj != undefined) {
			endFill();
		}
	}
}
_global.draw_circle = function(obj, x, y, r, line_obj, fill_obj) {
	obj = (obj == undefined) ? _root : obj;
	with (obj) {
		if (line_obj != undefined) {
			lineStyle(line_obj.size, line_obj.color, line_obj.alpha);
		}
		if (fill_obj != undefined) {
			beginFill(fill_obj.color, fill_obj.alpha);
		}
		moveTo(x - r, y);
		curveTo(x - r, y - r, x, y - r);
		curveTo(x + r, y - r, x + r, y);
		curveTo(x + r, y + r, x, y + r);
		curveTo(x - r, y + r, x - r, y);
		if (fill_obj != undefined) {
			endFill();
		}
	}
}
