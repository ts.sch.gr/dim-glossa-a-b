/*
################################
##	Bournazos Charilaos 2005  ##
################################
*/

_global.point = function(x, y){
	this.x = x;
	this.y = y;
}
_global.point.prototype.distanceTo = function(p) {
	return Math.sqrt(Math.pow(p.x - this.x, 2) + Math.pow(p.y - this.y, 2));
}
_global.point.prototype.toString = function() {
	return this.x + ", " + this.y;
}
_global.point.prototype.inRect = function(rect) {
	return ((this.x >= rect.xMin) && (this.x <= rect.xMax) && (this.y >= rect.yMin) && (this.y <= rect.yMax));
}
