function list_contains(aList, aElement, offsetStart) {	
	var result;
	if (offsetStart == undefined) {
		offsetStart = 0
	}
	var i = offsetStart
	for (i; i < aList.length; i++) {
		if (aList[i] == aElement || aList[i] === aElement) {
			result = i;
			break;
		}
	}
	return result;
}
_global.point = function (ax, ay) {
	this.x = ax;
	this.y = ay;
}

_global.distanceTo = function (p1, p2) {
	var A = p1.x - p2.x;
	var B = p1.y - p2.y;
	var C = Math.sqrt(Math.pow(A, 2) + Math.pow(B, 2));
	return C
}

_global.mouse_in_rect = function(minX, minY, maxX, maxY, aClip) {
	var res = false;
	if (aClip == undefined) {
		aClip = _root
	}
		if ((_root._xmouse >= minX + aClip._x && _root._xmouse <= maxX + aClip._x) && (_root._ymouse >= minY + aClip._y && _root._ymouse <= maxY + aClip._y)) {
			res = true
		}
	return res
}