//TODO: save data MOVE command
function drawingCanvas(canv) {
	canv.useHandCursor = false;
	//this.canvas = canv;
	var d = canv.getDepth();
	canv.swapDepths(-d);
	this.canvas = canv._parent.createEmptyMovieClip(canv._name + "_canvas", d);	
	this.canvas.sketch = canv;
	this.canvas.paper = this.canvas.createEmptyMovieClip("paper", 2);
	var bnd = canv.getBounds(canv._parent);
	draw_rect(this.canvas.paper, bnd.xMin, bnd.yMin, bnd.xMax, bnd.yMax, {size:0, color:0xCCCCCC, alpha: 100}, {color:0xFFFFFF, alpha:0});
	this.canvas.mask = canv.duplicateMovieClip(canv._name + "_mask", 3, {_x: canv._x, _y: canv._y});
	this.pointer = this.canvas.createEmptyMovieClip("pointer", 4);	
	draw_circle(this.pointer, 0, 0, 10, {size: 0, color: 0x000000, alpha: 100}, undefined);
	var bnd = canv.getBounds(this.canvas);
	draw_rect(this.canvas, bnd.xMin, bnd.yMin, bnd.xMax, bnd.yMax, undefined, {color:0x00FFFF, alpha:0});
	//bnd = this.canvas.getBounds(this.canvas);
	//draw_rect(this.canvas.mask, bnd.xMin, bnd.yMin, bnd.xMax, bnd.yMax, undefined, {color:0x00FF00, alpha:100});
	this.canvas.paper.setMask(this.canvas.mask);
	this.canvas.offset = new point(this.canvas._x, this.canvas._y);
	this.canvas.saveData = ""
	this.canvas.wClass = this;
	this.canvas.onPress = function() {
		this.clicking = true;
		this.wClass.onClick();
	};
	this.canvas.onRelease = this.canvas.onReleaseOutside = function() {
		this.clicking = false;
		this.last_point = undefined;
		this.wClass.onClicked();
	};
	this.canvas.execute = function(command, i) {
		var props = command.split(" ");
		switch (props[0]) {
		case "BRUSH":
			this.lastBrush = parseInt(props[1]);
			this.paper.lineStyle(this.lastBrush, this.lastColor, 100);
			break;
			case "COLOR":
			this.lastColor = parseFloat(props[1]);
			this.paper.lineStyle(this.lastBrush, this.lastColor, 100);
			break;
		case "POINT":
			var np = new point(parseFloat(props[1]), parseFloat(props[2]));
			if (this.last_point == undefined) {
				this.last_point = np;
				this.paper.moveTo(this.last_point.x, this.last_point.y);
				this.paper.lineTo(this.last_point.x, this.last_point.y);
			} else {
				this.paper.lineTo(np.x, np.y);
			}
			delete np;
			break;
		case "MOVE":
			var np = new point(parseFloat(props[1]), parseFloat(props[2]));
			this.last_point = np;
			this.paper.moveTo(this.last_point.x, this.last_point.y);
			break;
		default:
			var error = "Syntax Error Or Empty Line (" + ++i + "): " + command
			break;
		}
		if (error) {
			trace(error);
		} else {
			this.saveData += command + "\r";
		}
	};
	this.canvas.reset = function() {
		this.saveData = "";
		this.paper.clear();
		var bnd = this.sketch.getBounds(this.sketch._parent);
		draw_rect(this.paper, bnd.xMin, bnd.yMin, bnd.xMax, bnd.yMax, {size:0, color:0xCCCCCC, alpha: 100}, {color:0xFFFFFF, alpha:0});
	}
	this.canvas.restore = function(data) {
		this.reset();
		data = data.split("\r");
		var i = 0, j = data.length;
		while (i < j) {
			this.execute(data[i], i);
			i++;
		}
	};
	this.canvas.refresh = function() {
		if (this.paper.hitTest(_root._xmouse, _root._ymouse, true)) {
			if (!this.pointer._visible) {
				this.pointer._width = this.pointer._height = brush_size;
				this.pointer.startDrag(true);
				this.pointer._visible = true;
			}				
			if (this.clicking) {
				//Mouse.hide();
				//var aCol = RGB(_root.color_r.value, _root.color_g.value, _root.color_b.value);
				var aCol = selected_color;
				var np = new point(_xmouse - this.offset.x, _ymouse - this.offset.y);
				if ((np.x != this.lastSavedPoint.x) || (np.y != this.lastSavedPoint.y)) {
					this.lastSavedPoint = np;
					this.paper.lineStyle(brush_size, acol, 100);
					if (this.last_point == undefined) {
						this.last_point = np;
						this.saveData += "MOVE " + np.x + " " + np.y + "\r";
						this.paper.moveTo(this.last_point.x, this.last_point.y);
						this.paper.lineTo(this.last_point.x + .1, this.last_point.y + .1);
					} else {
						this.paper.lineTo(np.x, np.y);
					}
					if (brush_size != this.lastBrush) {
						this.lastBrush = brush_size;
						this.saveData += "BRUSH " + brush_size + "\r";
					}
					if (acol != this.lastColor) {
						this.lastColor = acol;
						this.saveData += "COLOR " + acol + "\r";
					}
					this.saveData += "POINT " + np.x + " " + np.y + "\r";
					updateAfterEvent();
				}
			} else {
				//Mouse.show();
			}
		} else {
			this.last_point = undefined;
			if (this.pointer._visible) {
				this.pointer.stopDrag();
				this.pointer._visible = false;
			}		
		}
	};
	this.updater = setInterval(this.canvas, "refresh", 1);
	this.destruct = function() {
	this.canvas.sketch._cursor = -1;
		this.canvas.pointer.stopDrag();
		this.canvas.pointer.removeMovieClip();
		this.canvas.paper.removeMovieClip();
		delete this.canvas.onRelease;
		delete this.canvas.onPress;
		this.canvas.removeMovieClip();
		this.canvas.mask.removeMovieClip();
		//this.canvas.sketch.removeMovieClip();
		clearInterval(this.updater);
		delete this.updater;
	};
}
