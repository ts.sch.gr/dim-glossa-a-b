
/*
xTracer substitutes the tracer from the authoring environment of flashMX in projector mode
it needs work !!!
*/

_global.xtrace = function(txt) {
	if (!_root.__xTracer) {
		_root.createEmptyMovieClip("__xTracer", 12312312);
		_root.__xTracer.createTextField("tf", 5000, 0, 10, 100, 60);
		_root.__xTracer.tf.multiline = _root.__xTracer.tf.wordWrap = true;
		_root.__xTracer.tf.background = 0xFFFFFF;
		_root.__xTracer.tf.border = true;
		_root.__xTracer.tf.type = "input";
		var xbtn = _root.__xTracer.createEmptyMovieClip("xbtn", 2);
		with (xbtn) {
			lineStyle(0, 0, 0);
			beginFill(0xFF0000, 100);
			moveTo(0, 0);
			lineTo(10, 0);
			lineTo(10, 10);
			lineTo(0, 10);
			lineTo(0, 0);
			endFill();
		}
		xbtn.onRelease = function() {
			__xTracer._visible = false;
		};
	}
	_root.__xTracer.tf.text += ((txt == undefined) ? "undefined" : txt) + "\r";
	_root.__xTracer._visible = true;
	trace(txt);
}
