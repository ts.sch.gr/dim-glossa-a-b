_root.debugged_times = 0;
_root.all_buttons = [];

function clip_rolls(aClip) {
	var bounds = aClip.getBounds(_root);
	var xf = Math.round(_root._xmouse) > bounds.xMin - 5 && Math.round(_root._xmouse) < bounds.xMax + 5;
	var yf = Math.round(_root._ymouse) > bounds.yMin - 5 && Math.round(_root._ymouse) < bounds.yMax + 5;
	var f = xf && yf;
	//var f = aClip.hitTest(_root._xmouse, _root._ymouse, false);
	return f;
}
function register_button(aButton, aStart, aEnd, aTarget) {
	aButton.starting = aStart;
	aButton.ending = aEnd;
	aButton.targeting = aTarget;
	aButton.onMouseMove = function() {
		if (this._currentframe > this.targeting - 1) {
			this.play();
			//trace("forced play");
		}
		if (!_root.clip_rolls(this)) {
			if (this._currentframe > this.starting && this._currentframe <= this.ending) {
				this.gotoAndPlay(this.targeting);
				_root.debugged_times++
				_root.debugged()
			}
		}
	};
	aButton.force_roll_out = function () {
		if (this._currentframe > this.starting && this._currentframe <= this.ending) {
			this.gotoAndPlay(this.targeting);
			_root.debugged_times++
			_root.debugged()
		}
	};
	_root.all_buttons.push(aButton);
	//setInterval(aButton, "updater", 5);
}
function all_buttons_off() {
	var i = 0;
	for (i; i < all_buttons.length; i++) {
		all_buttons[i].force_roll_out()
	}
}
