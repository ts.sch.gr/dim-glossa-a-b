//create the sound fx lib
_global.sfx = new Object();
sfx.pick = "pick";
sfx.click_tab = "click_tab";
sfx.place_ok = "place_ok";
sfx.place_wrong = "place_wrong";
sfx.level_unlocked = "level_unlocked";
sfx.well_done = "well_done"
sfx.good = ["good1", "good2", "good3", "good4"]
sfx.helped = "helped";
//initialize the sound fx engine
sfx.engine = new Sound(help)
sfx.play_sound = function(aSound) {
	this.sound = aSound;
	this.engine.stop();
	this.engine.attachSound(aSound);
	this.engine.start();
	this.playing = true;	
}
sfx.stop_sound = function() {
	this.engine.stop();
	this.playing = false;
	this.sound = undefined;
}

sfx.engine.onSoundComplete = function(aSound) {
	if (sfx.sound == sfx.well_done) {
		_root.after_all_sounds();
	}
	this.playing = false;
	this.sound = undefined;
}
////////////////////////////////////////////
helping = false;
function initialize_sound() {
	sound_engine = new Sound();
	sound_engine.onSoundComplete = function() {
		stop_help();		
	};
}
function stop_help() {
	if (helping) {
		sound_engine.stop();
		if (_root.help._currentframe > 1) {
			_root.help.gotoAndPlay(15);
		}
		helping = false;
		Mouse.removeListener(sound_mouse);
		after_sound();
	}	
}
function speak_help() {
	if (!helping) {
		sound_mouse = new Object();
		sound_mouse.onMouseUp = function() {
			if (!_root.traveling) {
				stop_help();
			}
		}
		initialize_sound();
		sound_engine.attachSound(help_sound);
		sound_engine.start();
		Mouse.addListener(sound_mouse);	
		helping = true;
	}
}
function introduce_riddle() {
	if (help_sound != undefined) {
		if (first_time != undefined) {
			clearInterval(first_time);
			first_time = undefined
			_root.help.gotoAndPlay(7)
		}
	}
}
if (!bypass_help) {
	//first_time = setInterval(introduce_riddle, 250);
}
bypass_help = false;