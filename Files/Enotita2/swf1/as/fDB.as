/*
##############################################################################################################################
##																															##
##	fDB class																												##
##	Bournazos Charilaos 2005																								##
##																															##
##	fDB is a simple kind of local database working with csv files															##
##	csv files are way more easy to produce than xml																			##
##																															##
##	just make an instance of the class and pass the location of the															##
##	database	e.g. myData_base = new fDB("./main");																		##
##	fDB reads the index.idx file in the folder wich holds the																##
##	information about the tables of the database (u need to write this)														##
##	after loading the data it raises the onLoaded event of the instance														##
##	u can overload this function with something like this																	##
##					myData_base.onLoaded = function() {																		##
##														<your code here>													##
##													}																		##
##	after loading you can access the data with three ways																	##
##	1. direct as arrays:																									##
##				var res = myData_base.tables[<table num>].recs[<record num>][<property num>];								##
##	2. using table alias:																									##
##				var res = myData_base.tables[myData_base.table_by_name(<table name>)].recs[<record num>][<property num>];	##
##	3. using SQL queries:																									##
##				var res = myData_base.exec("select * from <table name> where <condition>");									##
##		You can ask for properties like "select id, string...." but the condition can be only one and						##
##		less than, more than or equal (number or string). The fDB_bool.as contains a bool parser and you can use			##
##		more complex conditions, but it's too slow!																			##
##																															##
##############################################################################################################################
*/
//#include "./as/adds/array.as"
//#include "./as/chrlib.as"
function fDB(loc) {
	if (loc != undefined) {
		this.load(loc);
	}
}
_global.fDB_Err = function(err) {
	if (err == undefined) {
		return fDB_Error;
	} else {
		_global.fDB_Error = err;
	}
};
fDB_table = function (name) {
	this.name = name.toLowerCase();
	this.props = [];
	this.recs = [];
	this.count = function() {
		return this.recs.length;
	}
	this.addProp = function(name) {
		this.props.push(name.toLowerCase());
	};
	this.addRec = function(rec) {
		this.recs.push(rec);
	};
	this.select = function(props, crit) {
		// checks only equalities ;)
		var idx = [];
		var i = 0, ls;
		var res = [];
		if (props[0] == "*") {
			var j = this.props.length;
			while (i < j) {
				idx.push(i++);
			}
		} else {
			var j = props.length;
			while (i < j) {
				idx.push(ls = this.props.getPos(props[i++].toLowerCase()));
				if (ls == undefined) {
					fDB_Err("couldn't find property '" + props[i - 1] + "'.");
					return -1;
				}
			}
		}
		if (crit.indexOf("=") > -1) {
			var oper = "=";
		} else {
			var oper = "<>";
		}
		var cProp = this.props.getPos(crit.substring(0, crit.indexOf(oper)));
		var cVal = crit.substring(crit.lastIndexOf(oper) + 1, crit.length);
		var i = 0, j = this.recs.length, k, l = idx.length, r = -1;
		while (i < j) {
			if (this.recs[i][cProp].toLowerCase() == cVal) {
				k = 0;
				res.push([]);
				r++;
				while (k < l) {
					res[r].push(this.recs[i][idx[k]]);
					k++;
				}
			}
			i++;
		}
		return res;
	};
	this.toString = function(len) {
		if (len == undefined) {
			len = 10;
		}
		var res = "table: " + this.name + "\n";
		var i = 0, j = this.props.length;
		while (i < j) {
			res += " " + fix_str_len(this.props[i++], len);
		}
		res += "\n" + chr_rpt("-", (len + 1) * this.props.length) + "\n";
		var i = 0, j = this.recs.length;
		var l, k = this.recs[0].length;
		while (i < j) {
			l = 0;
			while (l < k) {
				res += " " + fix_str_len(this.recs[i][l], len);
				l++;
			}
			res += "\n";
			i++;
		}
		return res;
	};
};
fDB.prototype.onData_loaded = function(dta, reason, args) {
	switch (reason) {
	case "index load" :
		var tabNames = dta.split("\r\n");
		var i = 0, j = tabNames.length, nm;
		this.tables_loading = j;
		while (i < j) {
			this.tables.push(new fDB_table(nm = tabNames[i].substring(0, tabNames[i].lastIndexOf("."))));
			this.load_data(this.path + "/" + tabNames[i], "table load", nm);
			i++;
		}
		break;
	case "table load" :
		var recs = dta.split("\r\n");
		var tblname = chr_del(recs.shift(), "\t");
		var header = recs.shift().split("\t");
		var i = 0, j = header.length;
		var tbl = this.table_by_name(tblname);
		while (i < j) {
			if (header[i].length) {
				tbl.addProp(header[i]);
			}
			i++;
		}
		i = 0, j = recs.length;
		while (i < j) {
			if (recs[i].length) {
				tbl.addRec(recs[i].split("\t"));
			}
			i++;
		}
		if ((--this.tables_loading) == 0) {
			this.onLoaded();
		}
		break;
	}
};
fDB.prototype.load_data = function(src, reason) {
	this.queue.push(reason);
	this.loader = new LoadVars();
	this.loader.parent = this;
	this.loader.onLoad = function() {
		this.parent.onData_loaded(unescape(this).split("=&")[0], this.parent.queue.pop(), unescape(this).split("=&")[1]);
	};
	this.loader.load(src);
};
fDB.prototype.load_tables = function(index) {
};
fDB.prototype.load = function(location) {
	_global.fDB_Error = undefined;
	this.tables = [];
	this.queue = [];
	this.args = [];
	this.path = location;
	this.load_data(location + "/index.idx", "index load");
};
fDB.prototype.table_by_name = function(name) {
	var i = 0, j = this.tables.length;
	while (i < j) {
		if (this.tables[i++].name.toLowerCase() == name.toLowerCase()) {
			return this.tables[i - 1];
		}
	}
	return -1;
};
fDB.prototype.exec = function(que) {
	var org = que
	que = que.toLowerCase();
	var dta = que.split(" ");
	var com = dta[0];
	if (com == "select") {
		var props = chr_del(que.substring(6, que.indexOf(" from ")), " ");
		var hasCrit = (que.indexOf(" where "));
		var table = chr_del(que.substring(que.lastIndexOf("from ") + 5, (hasCrit > -1) ? hasCrit : que.length), " ");
		if (hasCrit > -1) {
			var criteria = chr_del(que.substring(que.lastIndexOf("where ") + 6, que.length), " ");
		}
		return this.table_by_name(table).select(props.split(","), criteria);
	} else if (com == "count") {
		var table = chr_del(dta[1], " ");
		return this.table_by_name(table).count();
	}
};
