function update_solution() {
	help_clip._visible = !help_clip._visible;
	if (++help_count == 20) {
		stop_solution();
	}
	updateAfterEvent();
}
function stop_solution() {
	help_clip._visible = true;
	help_clip._alpha = 0;
	help_clip = undefined;
	help_count = 0;
	clearInterval(help_interval);
	help_interval = undefined;
}
function give_help() {
	if (riddle.picked != undefined) {
		riddle.picked.clip._x = riddle.picked.origin_x;
		riddle.picked.clip._y = riddle.picked.origin_y;
		riddle.picked = undefined;
	}
	var i = 0;
	var found = false
	for (i; i < riddle.words.length; i++) {
		if (riddle.words[i].clip._alpha > 0) {
			var aid = riddle.words[i].id;
			var j = 0;
			for (j; j < riddle.places.length; j++) {
				if (help_interval == undefined) {
					if ((riddle.taken[riddle.places[j].place] == 0) && riddle.places[j].id == aid) {
						help_count = 0;
						help_clip = riddle.places[j].clip;
						help_clip._alpha = 100;
						help_interval = setInterval(update_solution, 100);
						sfx.play_sound("helped");
						found = true;		
					}
				}
				if (found) {
					break;
				}
			}
			if (found) {
				break;
			}
		}
	}
	if (riddle.found == riddle.limit) {
		_root.riddle_done();
	}
}