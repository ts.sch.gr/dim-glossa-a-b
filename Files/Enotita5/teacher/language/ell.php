<?php 

// ΓΕΝΙΚΑ
define('_RECORDS','Εμφάνιση εγγραφών');
define('_OF','εως');
define('_TOTAL','συνολικά');
define('_PREV','Προηγούμενη');
define('_NEXT','Επόμενη');
define('_CHOOSE','Επιλογή');
define('_CHOOSEACTIVITY','Επέλεξε Δραστηριότητα');
define('_NEWRECORD','Νέα Εγγραφή');
define('_EDIT','Επεξεργασία');
define('_DELETE','Διαγραφή');
define('_ACTIONS','Ενέργειες');
define('_SAVE','Αποθήκευση');
define('_UNAUTHORIZEDACCESS','Παράνομη Πρόσβαση');
define('_IMAGE','Εικόνα');
define('_CHANGEIMAGE','Αλλαγή Εικόνας');
define('_INSERTIMAGE','Εισαγωγή Εικόνας');
define('_DELETEIMAGE','Διαγραφή Εικόνας');
define('_IMAGEFILENAMEDESC','Το σύστημα θα εισάγει αυτόματα το προσωπικό σας ID στο όνομα της εικόνας');
define('_IMAGESFILENAMEDESC','Το σύστημα θα εισάγει αυτόματα το προσωπικό σας ID στα ονόματα των εικόνων');
define('_SOUND','Ήχος');
define('_CHANGESOUND','Αλλαγή Ήχου');
define('_INSERTSOUND','Εισαγωγή Ήχου');
define('_DELETESOUND','Διαγραφή Ήχου');
define('_SOUNDFILENAMEDESC','Το σύστημα θα εισάγει αυτόματα το προσωπικό σας ID στο όνομα του ήχου');
define('_SHOWINACTIVITY','Εμφάνιση');
define('_SHOWINACTIVITYDESC','Εμφάνιση ή όχι της συγκεκριμένης εγγραφής στη δραστηριότητα');

// LOGIN
define('_ID','ID');
define('_PASSWORD','Κωδικός');
define('_LOGIN','Εισαγωγή στο Σύστημα');
define('_LOGOUT','Έξοδος από το Σύστημα');
define('_CREATEACCOUNT','Δημιουργία Λογαριασμού');
define('_NOSUCHID','Το ID αυτό δεν υπάρχει στο σύστημα');
define('_WRONGPASSWORD','Ο κωδικός είναι λανθασμένος');

// UPLOAD
define('_MAXFILEBYTES','To αρχείο είναι μεγαλύτερο σε μέγεθος απο το επιτρεπόμενο όριο των');
define('_CHOOSEFILE','Παρακαλώ επιλέξτε αρχείο');
define('_STOREERROR','Δεν ήταν δυνατή η αποθήκευση του αρχείου');
define('_TYPENUMBERIMAGES','Παρακαλώ πληκρολογήστε τον αριθμό των εικόνων που θα αποστείλετε.');
define('_NUMBEREQUALSWORDS','Ο αριθμός τους πρέπει να είναι ίσος με τον αριθμό των λέξεων της ακροστιχίδας.');
define('_MAXNUMBER','(μέγιστος αριθμός 9)');
define('_LINEWORDSEQUALSIMAGES','Η σειρά των εικόνων πρέπει να είναι ίδια με τη σειρά των λέξεων της ακροστιχίδας');
define('_IMAGEFORMAT','Ο τύπος της εικόνας πρέπει να είναι τύπου .jpg');
define('_SOUNDFORMAT','Ο τύπος του ήχου πρέπει να είναι τύπου .mp3');

// REGISTER
define('_LASTNAME','Επώνυμο');
define('_FIRSTNAME','Όνομα');
define('_EMAIL','Ηλεκτρονική Διεύθυνση');
define('_SCHOOL','Σχολείο');
define('_RETYPEPASSWORD','Επαλήθευση Κωδικού');
define('_REGISTRATIONCOMPLETED','Η διαδικασία εγγραφής ολοκληρώθηκε');
define('_YOURID','Το προσωπικό σας ID είναι το: ');
define('_USESYSTEM','Με αυτό θα μπορείτε να χρησιμοποιήσετε τις λειτουργίες του συστήματος');
define('_GOTOLOGIN','Μετάβαση στην κεντρική σελίδα');

// ΜΠΑΛΟΝΙΑ
define('_INSERTBALOONS','ΕΙΣΑΓΩΓΗ ΔΕΔΟΜΕΝΩΝ ΓΙΑ ΤΗ ΔΡΑΣΤΗΡΙΟΤΗΤΑ "ΤΑ ΜΠΑΛΟΝΙΑ"');
define('_EDITBALOONS','ΕΠΕΞΕΡΓΑΣΙΑ ΔΕΔΟΜΕΝΩΝ ΓΙΑ ΤΗ ΔΡΑΣΤΗΡΙΟΤΗΤΑ "ΤΑ ΜΠΑΛΟΝΙΑ"');
define('_CORRECTWORDS','Σωστές Λέξεις');
define('_FALSEWORDS','Λανθασμένες Λέξεις');
define('_LETTER','Γράμμα');
define('_CORRECTWORDSDESC','Γράψτε εδώ τις σωστές λέξεις χωρισμένες με ;');
define('_FALSEWORDSDESC','Γράψτε εδώ τις λανθασμένες λέξεις χωρισμένες με ;');
define('_LETTERDESC','Γράψτε εδώ το γράμμα της δραστηριότητας');

// ΤΑΞΙΝΟΜΗΣΗ
define('_SIGN','Ταμπέλα');
define('_WORDS','Λέξεις');
define('_INSERTTAKSINOMISI','ΕΙΣΑΓΩΓΗ ΔΕΔΟΜΕΝΩΝ ΓΙΑ ΤΗ ΔΡΑΣΤΗΡΙΟΤΗΤΑ "ΤΑΞΙΝΟΜΗΣΗ"');
define('_EDITTAKSINOMISI','ΕΠΕΞΕΡΓΑΣΙΑ ΔΕΔΟΜΕΝΩΝ ΓΙΑ ΤΗ ΔΡΑΣΤΗΡΙΟΤΗΤΑ "ΤΑΞΙΝΟΜΗΣΗ"');
define('_SIGNDESC','Γράψτε εδώ το γράμμα της ταμπέλας');
define('_WORDSDESC1','Γράψτε εδώ τις λέξεις της ταμπέλας χωρισμένες με ;');

// ΣΥΛΛΑΒΕΣ ΜΠΕΡΔΕΥΤΗΚΑΝ
define('_WORD','Λέξη');
define('_SYLLABLES','Συλλαβές');
define('_INSERTSYLLABESMPERDEYTHKAN','ΕΙΣΑΓΩΓΗ ΔΕΔΟΜΕΝΩΝ ΓΙΑ ΤΗ ΔΡΑΣΤΗΡΙΟΤΗΤΑ "ΟΙ ΣΥΛΛΑΒΕΣ ΜΠΕΡΔΕΥΤΗΚΑΝ"');
define('_EDITSYLLABESMPERDEYTHKAN','ΕΠΕΞΕΡΓΑΣΙΑ ΔΕΔΟΜΕΝΩΝ ΓΙΑ ΤΗ ΔΡΑΣΤΗΡΙΟΤΗΤΑ "ΟΙ ΣΥΛΛΑΒΕΣ ΜΠΕΡΔΕΥΤΗΚΑΝ"');
define('_WORDDESC1','Γράψτε εδώ τη λέξη');
define('_SYLLABLESDESC1','Γράψτε εδώ τις συλλαβές χωρισμένες με ;');

// ΠΡΟΤΑΣΕΙΣ ΜΠΕΡΔΕΥΤΗΚΑΝ
define('_SENTENCE','Πρόταση');
define('_INSERTPROTASEISMPERDEYTHKAN','ΕΙΣΑΓΩΓΗ ΔΕΔΟΜΕΝΩΝ ΓΙΑ ΤΗ ΔΡΑΣΤΗΡΙΟΤΗΤΑ "ΟΙ ΠΡΟΤΑΣΕΙΣ ΜΠΕΡΔΕΥΤΗΚΑΝ"');
define('_EDITPROTASEISMPERDEYTHKAN','ΕΠΕΞΕΡΓΑΣΙΑ ΔΕΔΟΜΕΝΩΝ ΓΙΑ ΤΗ ΔΡΑΣΤΗΡΙΟΤΗΤΑ "ΟΙ ΠΡΟΤΑΣΕΙΣ ΜΠΕΡΔΕΥΤΗΚΑΝ"');
define('_SENTENCEDESC','Γράψτε εδώ την πρόταση');
define('_WORDSDESC2','Γράψτε εδώ τις μπερδεμένες λέξεις χωρισμένες με ;');

// ΦΤΙΑΧΝΩ ΛΕΞΕΙΣ
define('_BIGWORD','Μεγάλη Λέξη');
define('_SMALLWORDS','Μικρές Λέξεις');
define('_INSERTFTIAXNOLEKSEIS','ΕΙΣΑΓΩΓΗ ΔΕΔΟΜΕΝΩΝ ΓΙΑ ΤΗ ΔΡΑΣΤΗΡΙΟΤΗΤΑ "ΦΤΙΑΧΝΩ ΛΕΞΕΙΣ"');
define('_EDITFTIAXNOLEKSEIS','ΕΠΕΞΕΡΓΑΣΙΑ ΔΕΔΟΜΕΝΩΝ ΓΙΑ ΤΗ ΔΡΑΣΤΗΡΙΟΤΗΤΑ "ΦΤΙΑΧΝΩ ΛΕΞΕΙΣ"');
define('_BIGWORDDESC','Γράψτε εδώ τη μεγάλη λέξη');
define('_SMALLWORDSDESC','Γράψτε εδώ τις τριάδες των μικρών λέξεων. Οι μικρές λέξεις χωρισμένες με ; και οι τριάδες η μία κάτω απο την άλλη');

// ΒΡΙΣΚΩ ΣΥΛΛΑΒΕΣ
define('_SENTENCEWITHOUTSYLLABLES','Πρόταση Χωρίς Συλλαβές');
define('_INSERTBRISKOSYLLABES','ΕΙΣΑΓΩΓΗ ΔΕΔΟΜΕΝΩΝ ΓΙΑ ΤΗ ΔΡΑΣΤΗΡΙΟΤΗΤΑ "ΒΡΙΣΚΩ ΣΥΛΛΑΒΕΣ"');
define('_EDITBRISKOSYLLABES','ΕΠΕΞΕΡΓΑΣΙΑ ΔΕΔΟΜΕΝΩΝ ΓΙΑ ΤΗ ΔΡΑΣΤΗΡΙΟΤΗΤΑ "ΒΡΙΣΚΩ ΣΥΛΛΑΒΕΣ"');
define('_SENTENCEDESC2','Γράψτε εδώ την πρόταση. Στη θέση των συλλαβών που λείπουν βάλε τον χαρακτήρα _');
define('_SYLLABLESDESC2','Γράψτε εδώ τις συλλαβές που λείπουν χωρισμένες με ;');

// ΑΚΡΟΣΤΙΧΙΔΑ
define('_IMAGES','Εικόνες');
define('_CHANGEIMAGES','Αλλαγή Εικόνων');
define('_INSERTIMAGES','Εισαγωγή Εικόνων');
define('_DELETEIMAGES','Διαγραφή Εικόνων');
define('_INSERTAKROSTOIXIDA','ΕΙΣΑΓΩΓΗ ΔΕΔΟΜΕΝΩΝ ΓΙΑ ΤΗ ΔΡΑΣΤΗΡΙΟΤΗΤΑ "ΑΚΡΟΣΤΙΧΙΔΑ"');
define('_EDITAKROSTOIXIDA','ΕΠΕΞΕΡΓΑΣΙΑ ΔΕΔΟΜΕΝΩΝ ΓΙΑ ΤΗ ΔΡΑΣΤΗΡΙΟΤΗΤΑ "ΑΚΡΟΣΤΙΧΙΔΑ"');
define('_WORDSDESC3','Γράψτε εδώ τις λέξεις της ακροστιχίδας τη μία κάτω απο την άλλη');

// ΕΙΚΟΝΕΣ
define('_STORY','Ιστορία');
define('_WORDSENTENCE','Λέξη/Πρόταση');
define('_INSERTEIKONES','ΕΙΣΑΓΩΓΗ ΔΕΔΟΜΕΝΩΝ ΓΙΑ ΤΗ ΔΡΑΣΤΗΡΙΟΤΗΤΑ "ΕΙΚΟΝΕΣ"');
define('_EDITEIKONES','ΕΠΕΞΕΡΓΑΣΙΑ ΔΕΔΟΜΕΝΩΝ ΓΙΑ ΤΗ ΔΡΑΣΤΗΡΙΟΤΗΤΑ "ΕΙΚΟΝΕΣ"');
define('_WORDSENTENCEDESC','Γράψτε εδώ τη λέξη. Μπορείτε αν θέλετε να γράψετε και μικρές προτάσεις');
define('_STORYDESC','Γράψτε εδώ έναν αριθμό για την ιστορία. Οι εγγραφές που έχουν ίδιο αριθμό ιστορίας θα εμφανίζονται στην ίδια δραστηριότητα.');

// ΜΑΪΜΟΥ
define('_ERMINEIA','Ερμηνεία');
define('_ERMINEIADESC','Γράψτε εδώ την ερμηνεία της λέξης');
define('_INSERTMAIMOY','ΕΙΣΑΓΩΓΗ ΔΕΔΟΜΕΝΩΝ ΓΙΑ ΤΗ ΔΡΑΣΤΗΡΙΟΤΗΤΑ "Η ΜΑΪΜΟΥ"');
define('_EDITMAIMOY','ΕΠΕΞΕΡΓΑΣΙΑ ΔΕΔΟΜΕΝΩΝ ΓΙΑ ΤΗ ΔΡΑΣΤΗΡΙΟΤΗΤΑ "Η ΜΑΪΜΟΥ"');
define('_WORDDESC2','Γράψτε εδώ τη λέξη (Με κεφαλαία γράμματα). Μέγιστοι χαρακτήρες 7');

?>