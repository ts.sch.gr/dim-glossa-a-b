<?php

session_start();

header('Content-Type: text/html; charset=utf-8');

include "API.php";

include "language/ell.php";

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>ΤΟ ΕΡΓΑΣΤΗΡΙΟ ΤΟΥ ΔΑΣΚΑΛΟΥ - Εγγραφή</title>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">
<META name="Author" content="Tessera Multimedia S.A.">
<link href="styles/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="scripts/scripts.js"></script>
</head>

<body style="margin:0px;">


<br>

<div align="center">


<?php


if (!$_POST)
{

?>

<form name="registration_form" method="post" action="register.php" onSubmit="return(checkRegistration());">


<table width="340" border="0" cellpadding="4" cellspacing="2">
<tr>
	<td width="50%" align="right" class="td1"><?php echo _LASTNAME; ?>:&nbsp;</td>
	<td width="50%" align="left"  class="td2"><input id="lastname" name="lastname" type="text" maxlength="100" style="width:150px;"></td>
</tr>
<tr>
	<td width="50%" align="right" class="td1"><?php echo _FIRSTNAME; ?>:&nbsp;</td>
	<td width="50%" align="left"  class="td2"><input id="firstname" name="firstname" type="text" maxlength="100" style="width:150px;"></td>
</tr>
<tr>
	<td width="50%" align="right" class="td1"><?php echo _EMAIL; ?>:&nbsp;</td>
	<td width="50%" align="left"  class="td2"><input id="email" name="email" type="text" maxlength="100" style="width:150px;"></td>
</tr>
<tr>
	<td width="50%" align="right" class="td1"><?php echo _SCHOOL; ?>:&nbsp;</td>
	<td width="50%" align="left"  class="td2"><input id="school" name="school" type="text" maxlength="100" style="width:150px;"></td>
</tr>
<tr>
	<td width="50%" align="right" class="td1"><?php echo _PASSWORD; ?>:&nbsp;</td>
	<td width="50%" align="left"  class="td2"><input id="password1" name="password1" type="password" maxlength="11" style="width:80px;"></td>
</tr>
<tr>
	<td width="50%" align="right" class="td1"><?php echo _RETYPEPASSWORD; ?>:&nbsp;</td>
	<td width="50%" align="left"  class="td2"><input id="password2" name="password2" type="password" maxlength="11" style="width:80px;"></td>
</tr>
<tr>
	<td width="100%" align="center" colspan="2">
	<input type="submit" name="submit" value="<?php echo _CREATEACCOUNT; ?>">
	</td>
</tr>
</table>

</form>


<?php

}
else
{
	$cd = date('Y-m-d');

	$sql = "INSERT INTO teachers (lastname, firstname, email, school, password, reg_date) VALUES ('".$_POST['lastname']."', '".$_POST['firstname']."', '".$_POST['email']."', '".$_POST['school']."', '".$_POST['password1']."', '".$cd."')";
	$result =& $dbconn->Execute($sql);


	if (!$result)
	{
		echo $dbconn->ErrorMsg();
	}
	else
	{
		$id = mysql_insert_id();

		$_SESSION['teacher_id'] = $id;
        $_SESSION['isUser'] = 1;
        

		echo '
		<table width="200" border="0" cellpadding="4" cellspacing="2">
		<tr>
			<td width="100%" align="center" class="td1">'._REGISTRATIONCOMPLETED.'</td>
		</tr>
		<tr>
			<td width="100%" align="center" class="td2">'._YOURID.' <b>'.$id.'</b></td>
		</tr>
		<tr>
			<td width="100%" align="center" class="td3">'._USESYSTEM.'</td>
		</tr>
		</table>';
		
		echo '<br>';

		echo '<a href="index.php">'._GOTOLOGIN.'</a>';

		//echo '<SCRIPT type="text/javascript">setTimeout("redirectTo(\'main.php\');",2000);</SCRIPT>';
		
	}
}

?>

</div>

</body>

</html>