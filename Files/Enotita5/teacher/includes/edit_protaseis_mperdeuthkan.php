<?php 

	session_start();
	
	header('Content-Type: text/html; charset=utf-8');

	include "../API.php";

	include "../language/ell.php";

	if (!$_SESSION['isUser']) unauthorized(); 
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Επεξεργασία Στοιχείων</title>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">
<META name="Author" content="Tessera Multimedia S.A.">
<link href="../styles/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../scripts/scripts.js"></script>
</head>

<body style="margin:20px;">


<?php

// SQL UPDATE RECORDS /////////////////////////
if ($_POST)
{
	$sql = 'UPDATE protaseis_mperdeuthkan SET lekseis = "'.$_POST['lekseis'].'", image = "'.$_POST['image'].'", sound = "'.$_POST['sound'].'", show_record = '.$_POST['show_record'].' WHERE teacher_id = '.$_SESSION['teacher_id'].' AND id = '.$_GET['id'];

	$rs =& $dbconn->Execute($sql);

	if ($rs) echo '<SCRIPT language="Javascript">window.opener.document.forms[\'mu\'].submit();window.close();</SCRIPT>';
}


// SQL GET RECORDS /////////////////////////

$sql = 'SELECT lekseis, image, sound, show_record FROM protaseis_mperdeuthkan WHERE teacher_id = '.$_SESSION['teacher_id'].' AND id = '.$_GET['id'];
$rs =& $dbconn->GetRow($sql);
////////////////////////////////////////////


echo '	<form id="mc" name="mc" method="post" action="edit_protaseis_mperdeuthkan.php?id='.$_GET['id'].'">';

echo '	<table width="100%" border="0" cellpadding="4" cellspacing="2">
		<tr>
			<td width="100%" class="td6">'._EDITPROTASEISMPERDEYTHKAN.'</td>
		</tr>
		</table>';


if (empty($rs['image']))
{
	$image_link = '<a href="javascript:;" onclick="'.uploadPhotoLink('protaseis_mperdeuthkan').'">'._INSERTIMAGE.'</a>';
}
else
{
	$image_link = '<a href="javascript:;" onclick="'.uploadPhotoLink('protaseis_mperdeuthkan').'">'._CHANGEIMAGE.'</a><br><a href="javascript:;" onclick="'.deleteImage().'">'._DELETEIMAGE.'</a>';
}

if (empty($rs['sound']))
{
	$sound_link = '<a href="javascript:;" onclick="'.uploadSoundLink('protaseis_mperdeuthkan').'">'._INSERTSOUND.'</a>';
}
else
{
	$sound_link = '<a href="javascript:;" onclick="'.uploadSoundLink('protaseis_mperdeuthkan').'">'._CHANGESOUND.'</a><br><a href="javascript:;" onclick="'.deleteSound().'">'._DELETESOUND.'</a>';
}


echo '	<table width="100%" border="0" cellspacing="2" cellpadding="4">
		<tr>
			<td class="td7" width="110">'._WORDS.':</td>
			<td class="td2" width="255"><input type="text" id="lekseis" name="lekseis" style="width:250px;" value="'.$rs['lekseis'].'" maxlength="32"></td>
			<td class="td8">'._WORDSDESC2.'</td>
		</tr>
		<tr>
			<td class="td7" width="110">'._IMAGE.':</td>
			<td class="td2" width="255">'.getImageForEI($rs['image'],'protaseis_mperdeuthkan').'</td>
			<td class="td8">'.$image_link.'</td>
		</tr>
		<tr>
			<td class="td7" width="110">'._SOUND.':</td>
			<td class="td2" width="255">'.getSoundForEI($rs['sound']).'</td>
			</td>
			<td class="td8">'.$sound_link.'</td>
		</tr>
		<tr>
			<td class="td7" width="110">'._SHOWINACTIVITY.':</td>
			<td class="td2" width="255">'.showInActivity($rs['show_record'],'').'</td>
			<td class="td8">'._SHOWINACTIVITYDESC.'</td>
		</tr>
		<tr> 
			<td align="center" valign="middle" colspan="3">
			<input type="submit" id="buttonDo" name="buttonDo" value="'._SAVE.'">
			</td>
		</tr>
		</table>';

echo '	</form>';


?>

</BODY>

</HTML>