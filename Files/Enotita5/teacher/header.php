<?php

session_start();

//Includes
include "API.php";
include "language/ell.php";

header('Content-Type: text/html; charset=utf-8');


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>ΤΟ ΕΡΓΑΣΤΗΡΙΟ ΤΟΥ ΔΑΣΚΑΛΟΥ</title>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">
<META name="Author" content="Tessera Multimedia S.A.">
<link href="styles/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="scripts/scripts.js"></script>
</head>

<body style="margin:0px;">

<?php

if (!$_SESSION['isUser']) unauthorized();

$color1 = '#CCFFCC';
$color2 = '#FFFFFF';

echo '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="100%" align="center">';


?>