<?php 

include("settings.php");

$db = NewADOConnection('mysql');
$vars =& $_REQUEST;

if (get_magic_quotes_gpc()) 
{
    while (list($k, $v) = each($vars)) 
	{
        $vars[$k] = stripslashes($v);
    }
}

$db->PConnect($vars['host'], $vars['username'], $vars['password'], $vars['db']);

// WHICH VERSION OF MYSQL ?
// IF ABOVE 4.1, SET NAMES MUST BE USED
$server_info = mysql_get_server_info ();
$server_version = (int) $server_info{0};
$server_subversion = (int) $server_info{2};

if ($server_version >= 5)
{
	$db->Execute("SET NAMES 'utf8'");
}
else if ($server_version == 4 && $server_subversion > 0)
{
	$db->Execute("SET NAMES 'utf8'");
}
////////////////////////////////////////


$array = explode(';', $vars['query']);

foreach($array as $value)
{
    $rs = $db->Execute($value);

	if (!$rs) 
	{
		die ($db->ErrorMSG());
	}
	else 
	{   
		$buffer = "";
		
		if ($rs->RecordCount() > 0) 
		{
			$arr = $rs->FetchRow();
			
			do 
			{
				$buffer .= implode(FIELD_SEPARATOR, $arr);
				$buffer .= RECORD_SEPARATOR;
				
			} 
			while ($arr = $rs->FetchRow());
		}
		
		echo $buffer;
	}
}

?>
